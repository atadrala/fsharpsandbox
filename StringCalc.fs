﻿#if INTERACTIVE
#r "C:\lib\FParsecCS.dll"
#r "C:\lib\FParsec.dll"
#endif
open FParsec
open FParsec.CharParsers

let setDelimiter d = updateUserState(fun _ -> d) >>. preturn d
let getDelimiter = getUserState

let delimiterHeaderStart = pstring "//"
let delimiterHeaderEnd = pstring "\\n"
let delimiterStart = pchar '['
let delimiterEnd = pchar ']'

let parseLongDelimiter =
    let delimiter = between delimiterStart delimiterEnd (manyChars (noneOf "]" ))
    in between delimiterHeaderStart delimiterHeaderEnd (many delimiter)
let parseShortDelimiter = between delimiterHeaderStart delimiterHeaderEnd (anyChar |>> fun c -> [c.ToString()])
let defaultDelimiter : Parser<string list, string list> = preturn [","] 
let delimiterParser = attempt parseLongDelimiter <|> parseShortDelimiter <|> defaultDelimiter >>= fun c -> setDelimiter c

let numbersParser : Parser<int list, string list> =
         getDelimiter >>= (List.map pstring >> List.fold (<|>) pzero >> sepBy pint32)

let calculate str = 
    let parser = delimiterParser >>. numbersParser
    in match runParserOnString parser [] "" str with
        | Success(numbers,_ , _) -> numbers |>  List.filter (fun x -> x < 1000) |> List.sum
        | Failure(_,_,_) -> invalidArg str "Invalid"

[<EntryPoint>]
let main argv = 
    let result = calculate "//[***][!]\\n1!2***3"
    in printfn "Result = %d" result
    0
