﻿open Microsoft.FSharp.Collections
open System
open System.IO
open System.Linq
open System.Collections.Generic

let inline (++) (x:int option) (y:int option) =  
    match (x,y) with
    | (Some(a), Some(b)) -> Some (a+b)
    | _ -> None 

type OrElseBuilder() =
    member this.ReturnFrom(x) = x
    member this.Combine (a,b) = if a <> 0 then a else b
    member this.Delay(f) = f()
let orElse = new OrElseBuilder()

type Field = 
     Land
   | Mountain 
   | Wather
   | Unknown
   with member f.cost = 
               match f with 
               | Land -> Some(1)
               | Wather -> Some(2)
               | _ -> None

type Action =
      Left
    | Right
    | Top
    | Down
    | Detonation
    | Undefined

type Board = Field ``[,]``
type 'T ``[,]`` with 
    member b.width = b.GetLength(1)
    member b.height = b.GetLength(0)

let parseBoardFromFile fileName = 
    let parseField c =
             match c with
             | 'L' -> Land
             | 'M' -> Mountain 
             | 'W' -> Wather
             | _   -> invalidArg "c" "not proper field" 
    let (width::(height::lines)) = File.ReadLines(fileName) |> List.ofSeq
     in Array2D.init (int32 height) (int32 width) (fun r c -> parseField lines.[r].[c])

let getField (b:Board) x y = if x >=0 && x < b.width && y >=0 &&  y < b.height then b.[y,x] else Unknown

[<CustomEquality;CustomComparison>] 
type Step =
    {
        Distance: int option;
        Field: Field;
        Detonate: int;
        X:int;
        Y:int;
        Path: Action list;
    }
    override x.Equals(yobj) = 
        match yobj with
        | :? Step as y -> x.Distance = y.Distance && x.Field = y.Field && x.Detonate = y.Detonate && x.X = y.X && x.Y = y.Y
        | _ -> false
    override x.GetHashCode() = 10501 * hash x.Field ^^^ hash x.Distance ^^^ hash 9901 * x.Detonate ^^^ hash 8999 * x.X ^^^  hash 3229 * x.Y
    interface System.IComparable with
      member x.CompareTo yObj = 
          match yObj with
          | :? Step as y -> 
                orElse {
                    return! compare x.Distance y.Distance
                    return! compare x.Detonate y.Detonate
                    return! compare x.Field y.Field
                    return! compare x.X y.X 
                    return! compare x.Y y.Y
                }
          | _ -> invalidArg "yObj" "Cannot compare"

type Backtrack = Step option ``[,,]``
type 'T ``[,,]`` with 
    member b.width = b.GetLength(1)
    member b.height = b.GetLength(0)

type State = { Board:Board; Backtrack:Backtrack; Queue: Set<Step> }

let translate direction x y = 
    match direction with 
    | Top -> (x,y-1)
    | Down -> (x,y+1)
    | Left -> (x-1,y)
    | Right -> (x+1,y) 
    | _ -> invalidArg "direction" "Cannot translate in undefined direction"

let generateNextStep board step direction =
    let distance = step.Distance ++ step.Field.cost
    let x, y = translate direction step.X step.Y
    let field = getField board x y
     in { Distance = distance; Field = field; X = x; Y = y; Path = direction::step.Path; Detonate = step.Detonate }

let updateBacktrack state step =
    let backtrackStep = state.Backtrack.[step.Y, step.X, step.Detonate]
     in if backtrackStep.IsNone || backtrackStep.Value.Distance > step.Distance then 
            state.Backtrack.[step.Y, step.X, step.Detonate] <- Some step;
            {Board = state.Board; Backtrack = state.Backtrack; Queue = state.Queue.Add(step)}
        else state

let generateNextStepAfterDetonation board step (dir1, dir2) = 
    let getFieldAfterDetonation x y = match getField board x y with
                                      | Mountain -> Land
                                      | x -> x
    let x, y = translate dir1 step.X step.Y
    let field = getFieldAfterDetonation x y
    let x', y' = translate dir2 x y
    let field' = getFieldAfterDetonation x' y'
     in {
          Distance = step.Distance ++ Some(2) ++ step.Field.cost ++ field.cost;
          Field = field';
          X = x';
          Y = y';
          Path = dir2::dir1::Detonation::step.Path;
          Detonate = 1 
        }

let possibleDirs = [Left; Right; Down; Top] 
let detonationFrontierPaths = [(Left,Left); (Left,Top); (Top,Left); (Top,Top); (Top,Right); (Right,Top);
                               (Right,Right); (Right,Down); (Down,Right); (Down,Down); (Down,Left); (Left, Down)]

let makeMoveFromClosest state =
    let currentStep = state.Queue.MinimumElement
    let canMakeStep step = step.Field = Land || step.Field = Wather
    let canDetonate _ = currentStep.Detonate = 0
    let steps = possibleDirs |> List.map (generateNextStep state.Board currentStep) |> List.filter canMakeStep
    let detonationFrontier = detonationFrontierPaths |> List.filter canDetonate 
                             |> List.map (generateNextStepAfterDetonation state.Board currentStep) |> List.filter canMakeStep
    let withoutCurrentStep = {Board =state.Board; Backtrack = state.Backtrack; Queue = state.Queue.Remove currentStep}
     in List.fold updateBacktrack withoutCurrentStep (steps@detonationFrontier)

let solution state =
    let b = state.Backtrack 
    let sol1 = b.[ b.height-1, b.width-1, 0]
    let sol2 = b.[ b.height-1, b.width-1, 1]
     in if sol1.IsSome && (sol2.IsNone || sol1.Value.Distance < sol2.Value.Distance) then sol1
        else if sol2.IsSome then sol2
        else None

let achivedGoal state = 
    let sol = solution state
    let step = state.Queue.MinimumElement
     in sol.IsSome && sol.Value.Distance <= step.Distance
     
let extractSolution state =
    let sol = solution state 
    let directionToChar dir = 
        match dir with 
        | Top -> 'U'
        | Down-> 'D'
        | Left-> 'L'
        | Right -> 'R'
        | Detonation -> 'B'
        | _ -> invalidArg "dir" "Internal error"
    in List.rev sol.Value.Path |> List.map directionToChar |> System.String.Concat

let rec solve state =
    if achivedGoal state then extractSolution state 
    else solve <| makeMoveFromClosest state

[<EntryPoint>]
let main argv = 
    let board = parseBoardFromFile argv.[0];
    let startingStep = { Distance = Some(0); Field = getField board 0 0; X = 0; Y = 0; Path = []; Detonate = 0 }
    let initialState = {
                Board = board;
                Backtrack = Array3D.init board.height board.width 2 ( fun x y z -> None );
                Queue = Set<Step> [| startingStep |] 
            }
    printfn "%s" (solve initialState);
    0
